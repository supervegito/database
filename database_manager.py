import sqlite3
from sqlite3 import Error


class DatabaseManager(object):
    def __init__(self):
        self.database_file = 'databases/store.db'

    def create_connection(self):
        """ create a database connection to the SQLite database
            specified by db_file
        :param db_file: database file
        :return: Connection object or None
        """
        conn = None
        try:
            conn = sqlite3.connect(self.database_file)
        except Error as e:
            print(e)

        return conn

    def create_table(self, conn, create_table_sql):
        """ create a table from the create_table_sql statement
        :param conn: Connection object
        :param create_table_sql: a CREATE TABLE statement
        :return:
        """
        try:
            c = conn.cursor()
            c.execute(create_table_sql)
        except Error as e:
            print(e)

    def create_user(self, conn, user):
        """
        Create a new user into the users table
        :param conn:
        :param user:
        :return: user id
        """
        sql = ''' INSERT INTO users(username,password,first_name,last_name)
                  VALUES(?,?,?,?) '''
        cur = conn.cursor()
        cur.execute(sql, user)
        return cur.lastrowid

    def create_product(self, conn, product):
        """
        Create a new product
        :param conn:
        :param product:
        :return: product id
        """

        sql = ''' INSERT INTO products(name,price,quantity)
                  VALUES(?,?,?) '''
        cur = conn.cursor()
        cur.execute(sql, product)
        return cur.lastrowid

    def update_user(self, conn, user):
        """
        update username, password, first_name and last_name
        :param conn:
        :param user:
        :return: user id
        """
        sql = ''' UPDATE users
                  SET username = ? ,
                      password = ? ,
                      first_name = ?,
                      last_name = ?,
                  WHERE id = ?'''
        cur = conn.cursor()
        cur.execute(sql, user)
        conn.commit()

    def update_product(self, conn, product):
        """
        update name, price and quantity
        :param conn:
        :param product:
        :return: product id
        """
        sql = ''' UPDATE products
                  SET name = ? ,
                      price = ? ,
                      quantity = ?
                  WHERE id = ?'''
        cur = conn.cursor()
        cur.execute(sql, product)
        conn.commit()

    def create_users_and_products_tables(self, conn):
        sql_create_users_table = """ CREATE TABLE IF NOT EXISTS users (
                                                id integer PRIMARY KEY,
                                                username text NOT NULL UNIQUE,
                                                password text NOT NULL,
                                                first_name text NOT NULL,
                                                last_name text NOT NULL
                                            ); """

        sql_create_products_table = """ CREATE TABLE IF NOT EXISTS products (
                                            id integer PRIMARY KEY,
                                            name text NOT NULL UNIQUE,
                                            price real NOT NULL,
                                            quantity integer NOT NULL
                                        ); """

        # create tables
        if conn is not None:
            # create users table
            self.create_table(conn, sql_create_users_table)

            # create products table
            self.create_table(conn, sql_create_products_table)
        else:
            print("Error! cannot create the database connection.")

    @staticmethod
    def select_all_products(conn):
        """
        Query all rows in the products table
        :param conn: the Connection object
        :return:
        """
        cur = conn.cursor()
        cur.execute("SELECT * FROM products")

        rows = cur.fetchall()

        return rows

    @staticmethod
    def select_task_by_username(conn, username):
        """
        Query tasks by username
        :param conn: the Connection object
        :param username:
        :return:
        """
        cur = conn.cursor()
        cur.execute("SELECT * FROM users WHERE username=?", (username,))

        rows = cur.fetchone()

        return rows

    def get_all_products(self):
        conn = self.create_connection()

        with conn:
            return self.select_all_products(conn)

    def get_user(self, username):
        conn = self.create_connection()

        with conn:
            return self.select_task_by_username(conn, username)


def main():
    database_manager = DatabaseManager()

    # create a database connection
    conn = database_manager.create_connection()

    # create users and products tables
    database_manager.create_users_and_products_tables(conn)

    with conn:
        # create a new user
        user = ('root', 'root', 'Daniel', 'Eidlin')
        user_id = database_manager.create_user(conn, user)

        # products
        product_1 = ('Milk', 5.30, 10)
        product_2 = ('Cheese', 10.50, 17)
        product_3 = ('Cucumber', 0.50, 46)
        product_4 = ('Tomato', 0.76, 32)
        product_5 = ('My life', 0.0, 0)

        # create products
        database_manager.create_product(conn, product_1)
        database_manager.create_product(conn, product_2)
        database_manager.create_product(conn, product_3)
        database_manager.create_product(conn, product_4)
        database_manager.create_product(conn, product_5)


if __name__ == '__main__':
    main()
