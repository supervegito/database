import socket
import sys
import time


class Client(object):
    def __init__(self):
        self.my_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.my_socket.connect(('127.0.0.1', 4444))

    def authenticate(self, username, password):
        self.my_socket.send(str.encode(username))
        time.sleep(0.1)
        self.my_socket.send(str.encode(password))

        response = self.my_socket.recv(1024).decode('utf-8')
        print('\n' + response + '\n')
        return response


if __name__ == '__main__':
    client = Client()
    client.authenticate()
