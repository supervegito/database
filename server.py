import socket
import time

from database_manager import DatabaseManager


class Server(object):
    def __init__(self):
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.bind(('0.0.0.0', 4444))
        self.server_socket.listen(5)
        self.database_manager = DatabaseManager()

    def authenticate(self, client_socket):
        while True:
            username = client_socket.recv(1024).decode('utf-8')
            print(f'{username = }')
            password = client_socket.recv(1024).decode('utf-8')
            print(f'{password = }')
            user = self.database_manager.get_user(username)

            if user:
                user_password = user[1]
                if user_password == password:
                    products = self.database_manager.get_all_products()
                    client_socket.send(str(products).encode())
                    client_socket.close()
                    break
                else:
                    client_socket.send(str.encode('Invalid credentials!'))

            else:
                client_socket.send(str.encode('Invalid credentials!'))

    def main(self):
        new_socket, address = self.server_socket.accept()
        print(f'{address} has connected to server!')
        self.authenticate(new_socket)


if __name__ == '__main__':
    server = Server()
    server.main()
